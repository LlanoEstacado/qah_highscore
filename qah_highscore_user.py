import io
import sys
import requests

'''
Retrieving data from the Website
'''
def getData():
	print ("Getting data from https://quarantine.infino.me/users/ ....")
	response = requests.get('https://quarantine.infino.me/users/')
	content = response.content  # the bare .html data

	# cut out the ranking part
	start = str(content).find("Average time")
	end 	= str(content).find("h2Quarantine")
	content = content[start:end]

	# and clean it from .html syntax
	cleanCont = str(content).replace('<', '').replace('>', '').replace('\\n', '').replace('td', '').replace('/', '')

	userList = cleanCont.split("a href")
	shortList = []
	for i in range(0,len(userList),1):
		oneLine = userList[i].split('            ')
		if len(oneLine) < 6:
			continue
		elif len(oneLine) > 6:  # fix last line
			oneLine[4] = oneLine[4].split('tr')[0].replace(' ','')
		# tidy up usernames 
		oneLine[0] = oneLine[0].split("'")[-1][:-1] # however, all usernames end with 'a' --> delete it
		oneLine[4] = oneLine[4].replace(' ', '').replace('tr', '')
		shortList.append(oneLine[0:5]) # only keep necessary information
		
	return shortList


'''
Sort for lowest energy
'''
def bestEnergy(data, user):
	getauscht = True
	
	while getauscht:
		getauscht = False
		for i in range(1, len(data), 1):
			if float(data[i][4]) < float(data[i-1][4]):
				buf = data[i-1]
				data[i-1] = data[i]
				data[i] = buf
				getauscht = True
	# after sort print ranking
	found = False
	for i in range(0, len(data), 1):
		if data[i][0] == user: 
			print('Rank by Energy: \t\t', str(i+1), '\tProzent: ', str(int((i+1)/len(data) * 100)))
			found = True
			break
	# if the user does not exist --> print it
	if found == False:
		print("User <", user, "> not found !")

	# output results
	data_out = io.open('highscoreEnergy', 'w')
	for item in data:
		data_out.write(str(item[:]).replace('[', '').replace(']', '') + '\n')
	data_out.close()

'''
Sort for most results
'''
def mostResults(data, user):
	getauscht = True
	
	while getauscht:
		getauscht = False
		for i in range(1, len(data), 1):
			if float(data[i][1]) > float(data[i-1][1]):
				buf = data[i-1]
				data[i-1] = data[i]
				data[i] = buf
				getauscht = True
	# after sort print ranking
	found = False
	for i in range(0, len(data), 1):
		if data[i][0] == user:
			print('Rank by number of Results: \t', str(i+1), '\tProzent: ', str(int((i+1)/len(data) * 100)))
			found = True
			break
	# if the user does not exist --> print it
	if found == False:
		print("User <", user, "> not found !")

	# output results
	data_out = io.open('highscoreResults', 'w')
	for item in data:
		data_out.write(str(item[:]).replace('[', '').replace(']', '') + '\n')
	data_out.close()

if __name__ == '__main__':
	if len(sys.argv) != 2:
		print("Call: python qah_highscore_user.py [USER]")
		sys.exit(-1)
	# retrieve data from Website
	raw_data = getData()
	
	# print the desired user
	print('\n');
	print("Sort for",sys.argv[-1])
	
	# now sort for it
	bestEnergy(raw_data, sys.argv[-1]);
	mostResults(raw_data, sys.argv[-1]);
	
	print('\n');
	print("See complete Ranking in 'highscoreEnergy' and 'hisghscoreResults' respectively.")

	
	
