**Personal Highscore for Quarantine@Home**

Since [https://quarantine.infino.me/](url) has no function for displaying any highscore or ranking, I wrote this little Python3-Script to track my little contribution.

Feel free to use and/or extend it, but of course no warranty for anything! ;)

Usage: <br>
$ python qah_highscore_user.py [USER]